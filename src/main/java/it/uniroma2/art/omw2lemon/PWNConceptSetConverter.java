package it.uniroma2.art.omw2lemon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.NotDirectoryException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.RDFWriterRegistry;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.BasicWriterSettings;

import it.uniroma2.art.coda.converters.impl.TemplateBasedRandomIdGenerator;
import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.lime.model.vocabulary.ONTOLEX;
import it.uniroma2.art.lime.model.vocabulary.VARTRANS;
import it.uniroma2.art.omw2lemon.impls.Utilities;
import it.uniroma2.art.omw2lemon.vocabulary.WORDNET;
import net.sf.extjwnl.JWNLException;
import net.sf.extjwnl.data.POS;
import net.sf.extjwnl.data.Pointer;
import net.sf.extjwnl.data.PointerTarget;
import net.sf.extjwnl.data.PointerType;
import net.sf.extjwnl.data.Synset;
import net.sf.extjwnl.dictionary.Dictionary;

/**
 * This class implements the conversion of the conceptual backbone of WordNet 3.0 to the W3C OntoLex-Lemon
 * Model.
 * 
 * @see <a href="https://wordnet.princeton.edu/">About WordNet</a>
 * @see <a href="https://www.w3.org/2016/05/ontolex/#lexical-nets">The section on Lexical Nets of the W3C
 *      OntoLex-Lemon Model specification</a>
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class PWNConceptSetConverter {
	private String outputFilename;
	private String outputSynsetFileName;
	private String defaultNs;
	private Properties templates;
	private File ciliBaseDirectory;

	public static final int IDENTIFIER_WIDTH = 8;

	private static final String PWN30MAP_PATH = "ili-map-pwn30.tab";

	public static final String ILI_NAMESPACE = "http://ili.globalwordnet.org/ili/";

	public PWNConceptSetConverter(String outputFilename, String outputSynsetFileName, String defaultNs,
			String conceptSetIRITemplate, String synsetIRITemplate, File ciliBaseDirectory) {
		this.outputFilename = outputFilename;
		this.outputSynsetFileName = outputSynsetFileName;
		this.defaultNs = defaultNs;
		this.ciliBaseDirectory = ciliBaseDirectory;
		this.templates = new Properties();
		this.templates.setProperty("conceptSet", conceptSetIRITemplate);
		this.templates.setProperty("synset", synsetIRITemplate);
	}

	public void execute() throws JWNLException, ConversionException, IOException {

		Map<String, IRI> pwn30ToCILI = loadPWN30Map();

		Map<String, Properties> converterProperties = new HashMap<>();
		converterProperties.put(TemplateBasedRandomIdGenerator.CONVERTER_URI, templates);
		ValueFactory vf = SimpleValueFactory.getInstance();
		CODAContext codaCtx = Utilities.createCODAContext(defaultNs, converterProperties, vf);
		TemplateBasedRandomIdGenerator randIdGen = new TemplateBasedRandomIdGenerator();
		Dictionary pwn = Dictionary.getDefaultResourceInstance();

		String bcp47LanguageTag = "en";

		Map<PointerType, IRI> pointerType2SynsetRelation = new HashMap<>();
		pointerType2SynsetRelation.put(PointerType.HYPERNYM, WORDNET.HYPERNYM);
		pointerType2SynsetRelation.put(PointerType.HYPONYM, WORDNET.HYPONYM);
		pointerType2SynsetRelation.put(PointerType.PART_MERONYM, WORDNET.MERO_PART);
		pointerType2SynsetRelation.put(PointerType.CATEGORY_MEMBER, WORDNET.HAS_DOMAIN_TOPIC);
		pointerType2SynsetRelation.put(PointerType.SUBSTANCE_MERONYM, WORDNET.MERO_SUBSTANCE);
		pointerType2SynsetRelation.put(PointerType.PART_HOLONYM, WORDNET.HOLO_PART);
		pointerType2SynsetRelation.put(PointerType.CATEGORY, WORDNET.DOMAIN_TOPIC);
		pointerType2SynsetRelation.put(PointerType.MEMBER_HOLONYM, WORDNET.HOLO_MEMBER);
		pointerType2SynsetRelation.put(PointerType.INSTANCES_HYPONYM, WORDNET.INSTANCE_HYPONYM);
		pointerType2SynsetRelation.put(PointerType.ATTRIBUTE, WORDNET.ATTRIBUTE);
		pointerType2SynsetRelation.put(PointerType.USAGE, WORDNET.EXEMPLIFIES);
		pointerType2SynsetRelation.put(PointerType.INSTANCE_HYPERNYM, WORDNET.INSTANCE_HYPERNYM);
		pointerType2SynsetRelation.put(PointerType.REGION, WORDNET.DOMAIN_REGION);
		pointerType2SynsetRelation.put(PointerType.MEMBER_MERONYM, WORDNET.MERO_MEMBER);
		pointerType2SynsetRelation.put(PointerType.USAGE_MEMBER, WORDNET.IS_EXEMPLIFIED_BY);
		pointerType2SynsetRelation.put(PointerType.SUBSTANCE_HOLONYM, WORDNET.HOLO_SUBSTANCE);
		pointerType2SynsetRelation.put(PointerType.REGION_MEMBER, WORDNET.HAS_DOMAIN_REGION);
		pointerType2SynsetRelation.put(PointerType.ENTAILMENT, WORDNET.ENTAILS);
		pointerType2SynsetRelation.put(PointerType.VERB_GROUP, WORDNET.SIMILAR);
		pointerType2SynsetRelation.put(PointerType.SEE_ALSO, WORDNET.ALSO);
		pointerType2SynsetRelation.put(PointerType.SIMILAR_TO, WORDNET.SIMILAR);
		pointerType2SynsetRelation.put(PointerType.CAUSE, WORDNET.CAUSES);

		try {
			Set<String> wn30Synsets = new TreeSet<>();

			Model model = new LinkedHashModel();
			model.setNamespace(ONTOLEX.NS);
			model.setNamespace(SKOS.NS);
			model.setNamespace(WORDNET.NS);
			model.setNamespace(RDF.NS);
			model.setNamespace(VARTRANS.NS);

			IRI conceptSetIRI = vf.createIRI(defaultNs, "pwn30-conceptset");
			model.add(conceptSetIRI, RDF.TYPE, ONTOLEX.CONCEPT_SET);
			model.add(conceptSetIRI, DCTERMS.TITLE,
					vf.createLiteral("Princeton WordNet 3.0 Concept Set", "en"));

			for (POS pos : POS.getAllPOS()) {
				Iterator<Synset> it = pwn.getSynsetIterator(pos);
				while (it.hasNext()) {
					Synset s = it.next();
					long offset = s.getOffset();
					it.uniroma2.art.omw2lemon.model.wordnet.POS omwPOS = toOMWPOS(pos);
					String synsetId = String.format("%08d-%s", offset, omwPOS.toString());

					wn30Synsets.add(synsetId);

					IRI iliResource = pwn30ToCILI.get(synsetId);

					Map<String, Value> args = new HashMap<>();
					args.put("synset", vf.createLiteral(synsetId));
					IRI synsetIRI = randIdGen.produceURI(codaCtx, null, "synset", args);

					model.add(synsetIRI, RDF.TYPE, ONTOLEX.LEXICAL_CONCEPT);
					model.add(synsetIRI, WORDNET.HAS_PART_OF_SPEECH, omwPOS.toWordnetIRI());
					Resource definitionResource = vf.createBNode();
					String gloss = s.getGloss();
					if (!gloss.isEmpty()) {
						model.add(synsetIRI, SKOS.DEFINITION, definitionResource);
						model.add(definitionResource, RDF.VALUE, vf.createLiteral(gloss, bcp47LanguageTag));
					}
					if (iliResource == null) {
						System.err.println("Missing CILI entry for " + synsetId);
					} else {
						model.add(synsetIRI, OWL.SAMEAS, iliResource);
					}
					List<Pointer> pointerList = s.getPointers();

					boolean isTopConcept = true;

					for (Pointer pointer : pointerList) {
						PointerType pointerType = pointer.getType();
						PointerTarget target = pointer.getTarget();

						if (target instanceof Synset) {
							IRI pointerIRI = pointerType2SynsetRelation.get(pointerType);

							Synset targetSynset = (Synset) target;
							long targetSynsetOffset = targetSynset.getOffset();
							it.uniroma2.art.omw2lemon.model.wordnet.POS targetSynsetOmwPOS = toOMWPOS(
									targetSynset.getPOS());
							String targetSynsetId = String.format("%08d-%s", targetSynsetOffset,
									targetSynsetOmwPOS.toString());

							if (pointerIRI == null) {
								System.err.println("Unknown pointer type between synsets: " + pointerType
										+ "(" + pointer.getSource() + "  --->  " + pointer.getTarget() + ")");
							} else {
								Map<String, Value> args2 = new HashMap<>();
								args2.put("synset", vf.createLiteral(targetSynsetId));
								IRI targetIRI = randIdGen.produceURI(codaCtx, null, "synset", args2);

								// Adds reified relation
								BNode reifiedRelation = vf.createBNode();
								model.add(reifiedRelation, RDF.TYPE, VARTRANS.CONCEPTUAL_RELATION);
								model.add(reifiedRelation, VARTRANS.CATEGORY, pointerIRI);
								model.add(reifiedRelation, VARTRANS.SOURCE, synsetIRI);
								model.add(reifiedRelation, VARTRANS.TARGET, targetIRI);

								// In case of hypenym and hyponym, also adds non-reified SKOS relations
								if (WORDNET.HYPERNYM.equals(pointerIRI)) {
									model.add(synsetIRI, SKOS.BROADER, targetIRI);
									isTopConcept = false;
								} else if (WORDNET.HYPONYM.equals(pointerIRI)) {
									model.add(synsetIRI, SKOS.NARROWER, targetIRI);
								}
							}
						}
					}

					if (isTopConcept) {
						model.add(synsetIRI, SKOS.TOP_CONCEPT_OF, conceptSetIRI);
					} else {
						model.add(synsetIRI, SKOS.IN_SCHEME, conceptSetIRI);
					}
				}
			}

			RDFFormat outputFormat = RDFFormat
					.matchFileName(outputFilename, RDFWriterRegistry.getInstance().getKeys())
					.orElseThrow(() -> new IOException(
							"Could not recognize format for filename: " + outputFilename));
			File outputFile = new File(outputFilename);
			try (FileOutputStream out = new FileOutputStream(outputFile)) {
				RDFWriter outputWriter = Rio.createWriter(outputFormat, out);
				outputWriter.set(BasicWriterSettings.PRETTY_PRINT, true);
				Rio.write(model, outputWriter);
			}

			try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(outputSynsetFileName), StandardCharsets.UTF_8))) {
				for (String s : wn30Synsets) {
					out.write(s);
					out.write("\n");
				}
			}
		} finally {
			pwn.close();
		}
	}

	private Map<String, IRI> loadPWN30Map() throws IOException {
		File pwn30MapFile = new File(ciliBaseDirectory, PWN30MAP_PATH);

		ValueFactory vf = SimpleValueFactory.getInstance();
		Map<String, IRI> synset2cili = new HashMap<>();

		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(new FileInputStream(pwn30MapFile), Charset.forName("UTF-8")))) {
			String line;
			// ILI local name<tab>synset identifier
			while ((line = reader.readLine()) != null) {
				String[] components = line.split("\t");
				String synsetId = components[1];
				if (synsetId.endsWith("-s")) {
					synsetId = synsetId.substring(0, synsetId.lastIndexOf("-s")) + "-a";
				}
				synset2cili.put(synsetId, vf.createIRI(ILI_NAMESPACE, components[0]));
			}
		}
		return synset2cili;
	}

	private static it.uniroma2.art.omw2lemon.model.wordnet.POS toOMWPOS(POS pos) {
		switch (pos) {
		case ADJECTIVE:
			return it.uniroma2.art.omw2lemon.model.wordnet.POS.a;
		case ADVERB:
			return it.uniroma2.art.omw2lemon.model.wordnet.POS.r;
		case NOUN:
			return it.uniroma2.art.omw2lemon.model.wordnet.POS.n;
		case VERB:
			return it.uniroma2.art.omw2lemon.model.wordnet.POS.v;
		default:
			throw new IllegalArgumentException("unexpected pos " + pos);
		}
	}

	/**
	 * Entry point of the converter.
	 * 
	 * @param args
	 * @throws JWNLException
	 * @throws ConversionException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public static void main(String[] args)
			throws JWNLException, ConversionException, IOException, URISyntaxException {
		if (args.length != 2) {
			System.err.println(
					"Required arguments: <path to the Global WordNet Association Inter-Lingual Index> <default namespace>");
			System.exit(1);
		}

		String ciliPath = args[0];

		File ciliBaseDirectory = new File(ciliPath);
		if (!ciliBaseDirectory.exists()) {
			throw new FileNotFoundException(
					"The path to the Global WordNet Association Inter-Lingual Index does not exists");
		}

		if (!ciliBaseDirectory.isDirectory()) {
			throw new NotDirectoryException(ciliPath);
		}

		String defaultNs = args[1];

		new URI(defaultNs); // validates the provided namespace URI

		// Checkout this GIT repo: https://github.com/globalwordnet/ili
		new PWNConceptSetConverter("pwn-concepts.rdf", "pwn-synsets.txt", defaultNs, "pwn30-conceptSet",
				"${synset}", ciliBaseDirectory).execute();
	}
}
