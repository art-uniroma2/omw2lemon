package it.uniroma2.art.omw2lemon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.DC;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.model.vocabulary.XMLSchema;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.RDFWriterRegistry;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.BasicWriterSettings;

import it.uniroma2.art.coda.converters.impl.TemplateBasedRandomIdGenerator;
import it.uniroma2.art.coda.exception.ConversionException;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.lime.model.language.LanguageTagUtils;
import it.uniroma2.art.lime.model.vocabulary.LIME;
import it.uniroma2.art.lime.model.vocabulary.ONTOLEX;
import it.uniroma2.art.omw2lemon.impls.Utilities;
import it.uniroma2.art.omw2lemon.model.wordnet.Lexicon;
import it.uniroma2.art.omw2lemon.model.wordnet.POS;
import it.uniroma2.art.omw2lemon.model.wordnet.SynsetIdentifier;
import it.uniroma2.art.omw2lemon.model.wordnet.Word;

/**
 * This class implements the conversion of an individual wordnet from the Open Multilingual Wordnet project to
 * the W3C OntoLex-Lemon Model.
 * 
 * @see <a href="http://compling.hss.ntu.edu.sg/omw/">Open Multilingual Wordnet</a>
 * @see <a href="https://www.w3.org/2016/05/ontolex/#lexical-nets">The section on Lexical Nets of the W3C
 *      OntoLex-Lemon Model specification</a>
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class IndividualWordnetConverter {
	private String tabFilePath;
	private String defaultNs;
	private File outputFile;
	private File wn30SynsetFile;
	private Set<String> wn30Synsets;

	public IndividualWordnetConverter(File outputFile, String tabFilePath, String defaultNs,
			File wordnetSynsetFile) {
		this.outputFile = outputFile;
		this.tabFilePath = tabFilePath;
		this.defaultNs = defaultNs;
		this.wn30SynsetFile = wordnetSynsetFile;
		this.wn30Synsets = new HashSet<>();
	}

	public void execute() throws IOException, ConversionException {
		loadSynsets();

		File tabFile = new File(tabFilePath);

		Lexicon lexicon = new Lexicon();

		String wordnetName;
		String wordnet3digitLang;
		String wordnetBCP47Lang;
		String wordnetBCP47LangWithSubtags;
		String wordnetURL;
		String wordnetLicenseName;

		Optional<IRI> wordnetLangLexvo;
		Optional<IRI> wordnetLangLOC;

		TemplateBasedRandomIdGenerator randIdGen = new TemplateBasedRandomIdGenerator();
		Properties templates = new Properties();
		templates.setProperty("lexicon", "${project}-${lang}-lexicon");
		templates.setProperty("lexicalEntry", "${project}-${lang}-${lemma}-${pos}");
		templates.setProperty("form", "${project}-${lang}-${lemma}-${pos}-lemma");
		templates.setProperty("lexicalSense", "${project}-${lang}-${lemma}-${synset}");
		templates.setProperty("synset", "${synset}");

		Map<String, String> languageTranslation = new HashMap<>();
		languageTranslation.put("qcn", "zh-TW");

		Map<String, Properties> converterProperties = new HashMap<>();
		converterProperties.put(TemplateBasedRandomIdGenerator.CONVERTER_URI, templates);
		ValueFactory vf = SimpleValueFactory.getInstance();
		CODAContext codaCtx = Utilities.createCODAContext(defaultNs, converterProperties, vf);

		try (PrintWriter errorWriter = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(
						new File(outputFile.getParentFile(), outputFile.getName() + ".errors.txt")),
				StandardCharsets.UTF_8))) {
			try (BufferedReader tabReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(tabFile), Charset.forName("UTF-8")))) {
				String line = tabReader.readLine();

				// --- Process the metadata --- //

				// Metadata record: # name\tlang\turl\tlicense
				String[] headerComponents = line.split("\t");

				wordnetName = headerComponents[0].substring(1).trim(); // TODO: check that the field
																		// starts with "#"
				wordnet3digitLang = headerComponents[1];
				wordnetURL = headerComponents[2];
				wordnetLicenseName = headerComponents[3];

				String translatedLanguageWithSubtags = languageTranslation.getOrDefault(wordnet3digitLang,
						wordnet3digitLang);
				String translatedLanguage;
				String subtags; // includes possible "-"
				if (translatedLanguageWithSubtags.contains("-")) {
					int dashPosition = translatedLanguageWithSubtags.indexOf("-");
					translatedLanguage = translatedLanguageWithSubtags.substring(0, dashPosition);
					subtags = translatedLanguageWithSubtags.substring(dashPosition);
				} else {
					translatedLanguage = translatedLanguageWithSubtags;
					subtags = "";
				}
				wordnetBCP47Lang = LanguageTagUtils.toBCP47(translatedLanguage)
						.orElseThrow(() -> new IllegalArgumentException(
								"Unable to convert to BCP 47: " + translatedLanguage));
				wordnetBCP47LangWithSubtags = wordnetBCP47Lang + subtags;
				wordnetLangLexvo = LanguageTagUtils.toLexvo(translatedLanguage);
				wordnetLangLOC = LanguageTagUtils.toLOC(translatedLanguage);

				System.out.println("name = " + wordnetName);
				System.out.println("lang = " + wordnet3digitLang);
				System.out.println("BCP 47 = " + wordnetBCP47Lang);
				System.out.println("BCP 47 with subtags = " + wordnetBCP47LangWithSubtags);
				System.out.println("url = " + wordnetURL);
				System.out.println("wordnetLicenseName = " + wordnetLicenseName);

				// --- Process the data --- //

				/*
				 * offset-pos<tab>lang:lemma<tab>word offset-pos<tab>lang:def<tab>sid<tab>definition
				 * offset-pos<tab>lang:exe<tab>sid<tab>example
				 */

				// offset-pos is the Wordnet 3.0 synset identifier
				// we assume the fields for a synset are given contiguously

				/*
				 * The same lemma (word + pos) can be attached to different synsets, because of semantic
				 * ambiguity.
				 * 
				 * To avoid redundancy in the input, we first collect all entries and their senses, and then
				 * output them.
				 */

				String lemmaField = "lemma";
				String[] dataRecord = null;

				try {
					while ((line = tabReader.readLine()) != null) {
						line = line.trim();

						if (line.isEmpty())
							continue; // skip empty lines
						dataRecord = line.split("\t");

						// Skip anything other than lemma declarations
						if (!dataRecord[1].endsWith(lemmaField))
							continue;

						if (!dataRecord[1].contains(":")) {
							errorWriter.println("property non qualified by language");
						}

						if (dataRecord.length != 3) {
							errorWriter.println("Skip record with unexcepted number of entries: " + line);
							continue;
						}

						String wordnet30synsetId = dataRecord[0];
						String posChar = wordnet30synsetId.substring(wordnet30synsetId.lastIndexOf("-") + 1);

						// While the documentation states that satellite adjectives are treated as adjectives,
						// in some wordnets the postag "s" still occurs
						if (posChar.equals("s")) {
							errorWriter.println(
									"Change pos tag from \"s\" to \"a\" for synset: " + wordnet30synsetId);
							posChar = "a";

							// Update the synset identifier
							wordnet30synsetId = wordnet30synsetId.substring(0,
									wordnet30synsetId.lastIndexOf("-") + 1) + posChar;
						}

						if (!wn30Synsets.contains(wordnet30synsetId)) {
							errorWriter
									.println("Skip record with unrecognized synset id: " + wordnet30synsetId);
							continue;
						}
						POS pos = POS.valueOf(posChar);
						String lexicalForm = dataRecord[2];
						// System.out.println("synsetid " + wordnet30synsetId);
						// System.out.println("pos " + pos);
						// System.out.println("lexical form = " + lexicalForm);

						Word word = lexicon.getOrCreateWord(lexicalForm, pos);
						word.addSense(new SynsetIdentifier(wordnet30synsetId));
					}
				} catch (Exception e) {
					e.printStackTrace();
					errorWriter.println("line is =" + line + "//" + Arrays.toString(dataRecord));
				}
			}
		}

		Model model = new LinkedHashModel();
		model.setNamespace(DCTERMS.NS);
		model.setNamespace(ONTOLEX.NS);
		model.setNamespace(LIME.NS);
		model.setNamespace(XMLSchema.NS);
		model.setNamespace("schema", "http://schema.org/");
		model.setNamespace(RDFS.NS);

		Map<String, Value> lexiconArguments = new HashMap<>();
		lexiconArguments.put("project", vf.createLiteral(wordnetName));
		lexiconArguments.put("lang", vf.createLiteral(wordnetBCP47Lang));

		IRI lexiconIRI = randIdGen.produceURI(codaCtx, null, "lexicon", lexiconArguments);

		model.add(lexiconIRI, RDF.TYPE, LIME.LEXICON);
		model.add(lexiconIRI, RDFS.LABEL, vf.createLiteral(wordnetName));
		model.add(lexiconIRI, DCTERMS.TITLE, vf.createLiteral(wordnetName));
		model.add(lexiconIRI, vf.createIRI("http://schema.org/url"), vf.createIRI(wordnetURL));
		model.add(lexiconIRI, DCTERMS.RIGHTS, vf.createLiteral(wordnetLicenseName));
		model.add(lexiconIRI, LIME.LANGUAGE, vf.createLiteral(wordnetBCP47Lang));
		model.add(lexiconIRI, DC.LANGUAGE, vf.createLiteral(wordnetBCP47Lang));

		wordnetLangLexvo.ifPresent(langIRI -> model.add(lexiconIRI, DCTERMS.LANGUAGE, langIRI));
		wordnetLangLOC.ifPresent(langIRI -> model.add(lexiconIRI, DCTERMS.LANGUAGE, langIRI));

		Map<String, Value> wordArguments = new HashMap<>();
		wordArguments.put("project", vf.createLiteral(wordnetName));
		wordArguments.put("lang", vf.createLiteral(wordnetBCP47Lang));

		Map<String, Value> formArguments = new HashMap<>();
		formArguments.put("project", vf.createLiteral(wordnetName));
		formArguments.put("lang", vf.createLiteral(wordnetBCP47Lang));

		Map<String, Value> senseArguments = new HashMap<>();
		senseArguments.put("project", vf.createLiteral(wordnetName));
		senseArguments.put("lang", vf.createLiteral(wordnetBCP47Lang));

		for (Word word : lexicon.getWords()) {
			wordArguments.put("lemma", vf.createLiteral(word.getLexicalForm()));
			wordArguments.put("pos", vf.createLiteral(word.getPOS().toString()));

			formArguments.put("lemma", vf.createLiteral(word.getLexicalForm()));
			formArguments.put("pos", vf.createLiteral(word.getPOS().toString()));

			senseArguments.put("lemma", vf.createLiteral(word.getLexicalForm()));
			senseArguments.put("pos", vf.createLiteral(word.getPOS().toString()));

			IRI wordIRI = randIdGen.produceURI(codaCtx, null, "lexicalEntry", wordArguments);
			model.add(wordIRI, RDF.TYPE, ONTOLEX.LEXICAL_ENTRY);
			wordnetLangLexvo.ifPresent(langIRI -> model.add(wordIRI, DCTERMS.LANGUAGE, langIRI));
			wordnetLangLOC.ifPresent(langIRI -> model.add(wordIRI, DCTERMS.LANGUAGE, langIRI));
			model.add(wordIRI, LIME.LANGUAGE, vf.createLiteral(wordnetBCP47Lang));
			model.add(lexiconIRI, LIME.ENTRY, wordIRI);
			BNode canonicalFormResource = vf.createBNode();
			model.add(wordIRI, ONTOLEX.CANONICAL_FORM, canonicalFormResource);
			model.add(canonicalFormResource, ONTOLEX.WRITTEN_REP,
					vf.createLiteral(word.getLexicalForm(), wordnetBCP47LangWithSubtags));

			for (SynsetIdentifier synsetIdentifier : word.getSenses()) {
				senseArguments.put("synset", vf.createLiteral(synsetIdentifier.getId()));
				IRI senseIRI = randIdGen.produceURI(codaCtx, null, "lexicalSense", senseArguments);

				model.add(senseIRI, RDF.TYPE, ONTOLEX.LEXICAL_SENSE);
				model.add(wordIRI, ONTOLEX.SENSE, senseIRI);

				Map<String, Value> synsetArguments = new HashMap<>();
				synsetArguments.put("synset", vf.createLiteral(synsetIdentifier.getId()));
				IRI synsetIRI = randIdGen.produceURI(codaCtx, null, "synset", synsetArguments);

				model.add(wordIRI, ONTOLEX.EVOKES, synsetIRI);
				model.add(synsetIRI, ONTOLEX.LEXICALIZED_SENSE, senseIRI);
			}

		}
		RDFFormat outputFormat = RDFFormat
				.matchFileName(outputFile.getName(), RDFWriterRegistry.getInstance().getKeys())
				.orElseThrow(() -> new IOException(
						"Could not infer file format for file: " + outputFile.getName()));
		try (FileOutputStream out = new FileOutputStream(outputFile)) {
			RDFWriter outputWriter = Rio.createWriter(outputFormat, out);
			outputWriter.set(BasicWriterSettings.PRETTY_PRINT, true);

			Rio.write(model, outputWriter);
		}
	}

	private void loadSynsets() throws FileNotFoundException, IOException {
		try (BufferedReader synsetReader = new BufferedReader(
				new InputStreamReader(new FileInputStream(this.wn30SynsetFile), Charset.forName("UTF-8")))) {
			String synsetId;
			while ((synsetId = synsetReader.readLine()) != null) {
				wn30Synsets.add(synsetId);
			}
		}
		System.out.println("Read " + wn30Synsets.size() + " synsets");
	}
}
