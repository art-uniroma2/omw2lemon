package it.uniroma2.art.omw2lemon.impls;

import java.util.Map;
import java.util.Properties;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.base.RepositoryConnectionWrapper;

import it.uniroma2.art.coda.interfaces.CODAContext;

public class Utilities {

	public static CODAContext createCODAContext(String defaultNs, Map<String, Properties> converterProperties, ValueFactory vf) {
		CODAContext codaCtx = new CODAContext(new RepositoryConnectionWrapper(null) {
			public String getNamespace(String prefix)
					throws org.eclipse.rdf4j.repository.RepositoryException {
				if (prefix.equals("")) {
					return defaultNs;
				} else {
					throw new UnsupportedOperationException();
				}
			};
	
			@Override
			public boolean hasStatement(Resource subj, IRI pred, Value obj, boolean includeInferred,
					Resource... contexts) throws RepositoryException {
				return false;
			}
	
			@Override
			public boolean hasStatement(Statement st, boolean includeInferred, Resource... contexts)
					throws RepositoryException {
				// TODO Auto-generated method stub
				return false;
			}
	
			public org.eclipse.rdf4j.model.ValueFactory getValueFactory() {
				return vf;
			}
		}, converterProperties);
		return codaCtx;
	}

}
