package it.uniroma2.art.omw2lemon.model.wordnet;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Lexicon {
	private final Map<WordIdentifier, Word> words;

	public Lexicon() {
		this.words = new HashMap<>();
	}

	public Optional<Word> getWord(WordIdentifier id) {
		return Optional.ofNullable(words.get(id));
	}

	public Optional<Word> getWord(String lexicalForm, POS pos) {
		return getWord(new WordIdentifier(lexicalForm, pos));
	}
	
	public Collection<Word> getWords() {
		return words.values();
	}
	
	public Word getOrCreateWord(String lexicalForm, POS pos) {
		Optional<Word> wordHolder = getWord(lexicalForm, pos);
		
		if (wordHolder.isPresent()) {
			return wordHolder.get();
		}
		
		WordIdentifier wordId = new WordIdentifier(lexicalForm, pos);
		Word word = new Word(wordId);
		words.put(wordId, word);
		
		return word;
	}
}
