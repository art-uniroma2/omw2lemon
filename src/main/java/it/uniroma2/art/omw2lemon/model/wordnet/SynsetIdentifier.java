package it.uniroma2.art.omw2lemon.model.wordnet;

import java.util.Objects;

public class SynsetIdentifier {
	private final String id;

	public SynsetIdentifier(String id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (obj == this) {
			return true;
		}

		if (obj.getClass() != getClass()) {
			return false;
		}

		SynsetIdentifier objCast = (SynsetIdentifier) obj;

		return Objects.equals(id, objCast.id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	public String getId() {
		return id;
	}
}
