package it.uniroma2.art.omw2lemon.model.wordnet;

public class Synset {
	private final String id;
	
	public Synset(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
}
