package it.uniroma2.art.omw2lemon.model.wordnet;

import org.eclipse.rdf4j.model.IRI;

import it.uniroma2.art.omw2lemon.vocabulary.WORDNET;
public enum POS {
	a(WORDNET.ADJECTIVE), v(WORDNET.VERB), n(WORDNET.NOUN), r(WORDNET.ADVERB);

	private IRI wordnetIRI;

	private POS(IRI wordnetIRI) {
		this.wordnetIRI = wordnetIRI;
	}

	public IRI toWordnetIRI() {
		return wordnetIRI;
	}
}
