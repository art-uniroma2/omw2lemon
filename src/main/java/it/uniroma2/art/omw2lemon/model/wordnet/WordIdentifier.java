package it.uniroma2.art.omw2lemon.model.wordnet;

import java.util.Objects;

public class WordIdentifier {
	private final String lexicalForm;
	private final POS pos;

	public WordIdentifier(String lexicalForm, POS pos) {
		this.lexicalForm = lexicalForm;
		this.pos = pos;
	}

	public String getLexicalForm() {
		return lexicalForm;
	}

	public POS getPos() {
		return pos;
	}

	@Override
	public int hashCode() {
		return Objects.hash(lexicalForm, pos);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (obj == this) {
			return true;
		}

		if (obj.getClass() != getClass()) {
			return false;
		}

		WordIdentifier objCast = (WordIdentifier) obj;

		return Objects.equals(lexicalForm, objCast.getLexicalForm()) && Objects.equals(pos, objCast.getPos());
	}
}
