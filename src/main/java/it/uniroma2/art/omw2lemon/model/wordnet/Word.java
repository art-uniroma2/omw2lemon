package it.uniroma2.art.omw2lemon.model.wordnet;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;

public class Word {

	private final WordIdentifier id;
	private final List<SynsetIdentifier> senses;

	public Word(WordIdentifier id) {
		this.id = id;
		this.senses = new ArrayList<>();
	}

	public String getLexicalForm() {
		return id.getLexicalForm();
	}

	public POS getPOS() {
		return id.getPos();
	}

	public void addSense(SynsetIdentifier synsetIdentifier) {
		senses.add(synsetIdentifier);
	}

	public List<SynsetIdentifier> getSenses() {
		return senses;
	}
	
}
