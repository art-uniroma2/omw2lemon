package it.uniroma2.art.omw2lemon.model.wordnet;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ConceptSet {
	private final Map<SynsetIdentifier, Synset> synsets;

	public ConceptSet() {
		synsets = new HashMap<>();
	}

	public Optional<Synset> getSynset(SynsetIdentifier id) {
		return Optional.ofNullable(synsets.get(id));
	}

	public Collection<Synset> getSynsets() {
		return synsets.values();
	}

	public Synset getOrCreateSynset(String synsetId) {
		SynsetIdentifier synsetIdObj = new SynsetIdentifier(synsetId);
		Optional<Synset> synsetHolder = getSynset(synsetIdObj);

		if (synsetHolder.isPresent()) {
			return synsetHolder.get();
		}

		Synset synset = new Synset(synsetId);

		synsets.put(synsetIdObj, synset);
		return synset;
	}

}
