package it.uniroma2.art.omw2lemon;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.NotDirectoryException;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import it.uniroma2.art.coda.exception.ConversionException;

/**
 * This class implements converted the wordnet from the Open Multilingual Wordnet project to the W3C
 * OntoLex-Lemon Model (using {@link IndividualWordnetConverter}).
 * 
 * @see <a href="http://compling.hss.ntu.edu.sg/omw/">Open Multilingual Wordnet</a>
 * @see <a href="https://www.w3.org/2016/05/ontolex/#lexical-nets">The section on Lexical Nets of the W3C
 *      OntoLex-Lemon Model specification</a>
 * 
 * @author <a href="mailto:fiorelli@info.uniroma2.it">Manuel Fiorelli</a>
 *
 */
public class OMWConverter {
	/**
	 * Entry point of the converter.
	 * 
	 * @param args
	 * @throws ConversionException
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public static void main(String[] args) throws ConversionException, IOException, URISyntaxException {
		if (args.length != 3) {
			System.err.println(
					"Required arguments: <path to the directory contaning the data of the Open Multilingual Wordnet> <default namespace> <path to the file containg the list of exported synsets>");
			System.exit(1);
		}

		String omwPath = args[0];

		File omwDirectory = new File(omwPath);
		if (!omwDirectory.exists()) {
			throw new FileNotFoundException(
					"The path to the directory containing the data of the Open Multilingual Wordnet does not exist");
		}

		if (!omwDirectory.isDirectory()) {
			throw new NotDirectoryException(omwPath);
		}

		String defaultNs = args[1];

		new URI(defaultNs); // validates the provided namespace URI

		File wn30SynsetFile = new File(args[2]);

		if (!wn30SynsetFile.exists()) {
			throw new FileNotFoundException(
					"The path to the file containing the list of exported synsets does not exist");
		}

		File outputBaseDir = new File(".");
		int count = 0;
		for (File wnDir : omwDirectory.listFiles((FileFilter) DirectoryFileFilter.INSTANCE)) {
			for (File dataFile : wnDir
					.listFiles((FilenameFilter) new RegexFileFilter("wn-data-[a-zA-Z]*\\.tab"))) {
				String fileName = dataFile.getName();
				String lang = fileName.substring(fileName.lastIndexOf("-") + 1, fileName.lastIndexOf("."));
				count++;
				try {
					new IndividualWordnetConverter(
							new File(outputBaseDir, String.format("wn_%02d_%s.rdf", count, lang)),
							dataFile.getAbsolutePath(), defaultNs, wn30SynsetFile).execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
