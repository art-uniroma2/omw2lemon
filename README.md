Open Multilingual Wordnet to W3C OntoLex-Lemon Model
====================================================

This project provides a converter for the [Open Multilingual Wordnet](http://compling.hss.ntu.edu.sg/omw/) to
the [W3C OntoLex-Lemon Model](https://www.w3.org/2016/05/ontolex/).

Requirements
------------

This project uses [Apache Maven](https://maven.apache.org/) as a software project management and comprehension
tool.

By using Maven, almost every software dependency is retrieved automatically from the [Maven Central Repository](https://search.maven.org/) with the exception of the [LIME API](https://bitbucket.org/art-uniroma2/lime-api/),
which should be build from sources (e.g. by executing a `mvn clean install` on its sources).

In addition to software dependencies, this project requires that:
 * the [Global WordNet Association Inter-Lingual Index](https://github.com/globalwordnet/ili) is cloned to a
   directory in the local file system (e.g. `D:\Wordnets\Inter-Lingual Index\ili`)
 * the archive containing [data for all of the wordnets](http://compling.hss.ntu.edu.sg/omw/all.zip) is
   downloaded from the Open Multilingual Wordnet site and unzipped to a directory in the local file system
   (e.g. `D:\Wordnets\Open Multilingual Wordnet\all\wns` is the directory that contains a number   
   of directories for different wordnet projects).

Build
-----

When every requirement is satisfied, the project can be build using Maven, by executing the following command:

	mvn clean install
	
Run the conversion
------------------

The conversion consists of two parts: i) convert the conceptual backbone of WordNet 3.0, ii) convert the
wordnets aggregated by the Open Multilingual Wordnets project.

**Convert the conceptual backbone**

The converter is provided by the class `it.uniroma2.art.omw2lemon.PWNConceptSetConverter`, the
`main` method of which expects to arguments:
 * the path where the Inter-Lingual Index was cloned
 * the default namespace to use (e.g. http://art.uniroma2.it/pmki/omw/)
 
The converter can be executed using the `exec` Maven plugin, as follows (remember to change first argument
according to your configuration):

	mvn exec:java -Dexec.mainClass=it.uniroma2.art.omw2lemon.PWNConceptSetConverter -Dexec.arguments="D:\Wordnets\Inter-Lingual Index\ili,http://art.uniroma2.it/pmki/omw/"
	
The command above will create two files:
  * `pwn-concepts.rdf` containing an RDF/XML serialization of the conversion
  * `pwn-synsets` containing the list of synsets included in the export

**Convert the wordnets aggregated by the Open Multilingual Wordnets project**

The converter is provided by the class `it.uniroma2.art.omw2lemon.OMWConverter`, the `main`
method of which expects to arguments:
 * the path to the directory were data for all wordnets is contained (in different subdirectories)
 * the default namespace to use (e.g. http://art.uniroma2.it/pmki/omw/)
 * the path to the file containing the list of the synsets included in the conceptual backbone
   (i.e. the file `pwn-synsets.txt` produced during the conversion of the conceptual backbone)
   
The converter can be executed using the `exec` Maven plugin, as follows (remember to change first argument
and the third one according to your configuration):

	mvn exec:java -Dexec.mainClass=it.uniroma2.art.omw2lemon.OMWConverter -Dexec.arguments="D:/Wordnets/Open Multilingual Wordnet/all/wns,http://art.uniroma2.it/pmki/omw/"
	
The command above will create a number of files named `omw_xx.rdf` containing an RDF/XML serialization of
the conversion of each wordnet (currently, `xx` will range between 00 and 34) Additionally, for each of these
files a corrsponding file ending with `.errors.txt` is created to hold error information

